from datetime import datetime
import glob
import os
import re
from subprocess import Popen
import time
from urllib import urlopen, urlencode

try:
    from RPi import GPIO
except RuntimeError:
    print 'Not running from a Raspberry pi. Cannot use class Brightness.'


class Temperature(object):

    def __init__(self):
        self._load_kernel_modules()
        self.file_path = self._get_file_path()

    def _load_kernel_modules(self):
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')

    def _get_file_path(self):
        device_list = glob.glob('/sys/bus/w1/devices/28*')
        if not device_list:
            raise IOError('No signal from temperature sensor')
        return os.path.join(device_list[0], 'w1_slave')

    def get_temperature(self):
        with open(self.file_path, 'r') as f:
            match = re.search(r't=(?P<temperature>\d+)$', f.read())
            return float(match.groupdict().get('temperature', 0)) / 1000


class Brightness(object):

    pin = 18

    def __init__(self):
        # Tell GPIO library to use Broadcom GPIO references
        GPIO.setmode(GPIO.BCM)

    def discharge_capacitor(self):
        try:
            GPIO.setup(self.pin, GPIO.OUT)
        except RuntimeError:
            raise RuntimeError('Brightness sensor requires root authentication')

        GPIO.output(self.pin, GPIO.LOW)
        time.sleep(0.1)  # Takes time to discharge capacitor

    def get_brightness(self):
        self.discharge_capacitor()
        GPIO.setup(self.pin, GPIO.IN)

        measurement = 0
        while GPIO.input(self.pin) == GPIO.LOW:
            measurement += 1
            time.sleep(0.001)
        return measurement


class Video(object):
    '''Requires streamer to be installed'''

    video_folder = '../media/'
    device = '/dev/video0'
    time = '00:00:10'  # 10 seconds

    def __init__(self):
        if not os.path.exists(self.video_folder):
            os.mkdir(self.video_folder)
        self._generate_filename()

    def _generate_filename(self):
        self.basename = datetime.now().strftime('gangradur_%s')
        self.basepath = os.path.join(self.video_folder, self.basename)
        self.avipath = '%s.avi' % self.basepath
        self.oggpath = '%s.ogg' % self.basepath
        self.oggfile = '%s.ogg' % self.basename
        return self.avipath

    def record(self, time=None):
        command = [
            'streamer',
            '-c', self.device,
            '-f', 'rgb24',
            '-r', '3',
            '-t', time or self.time,
            '-o', self.avipath
        ]
        with open(os.devnull, 'w') as devnull:
            self._recording_process = Popen(command, stdout=devnull,
                                            stderr=devnull)

    def wait_until_recording_finishes(self):
        self._recording_process.wait()

    def compress_and_upload_video(self):
        compress_command = ['ffmpeg', '-i', self.avipath, '-acodec',
                            'libvorbis', '-ac', '1', '-b', '768k',
                            self.oggpath]
        self._compress_process = Popen(compress_command)
        self._compress_process.wait()
        scp_addr = 'ubuntu@46.149.18.8:code/blog/blog/media/tolvel/videos/'
        scp_command = ['scp', self.oggpath, scp_addr]
        self._scp_process = Popen(scp_command)
        self._scp_process.wait()


class Motor(object):

    def forward(timeout=None):
        raise NotImplementedError

    def backwrads(timeout=None):
        raise NotImplementedError

    def stop(timeout=None):
        raise NotImplementedError

    def relax():
        raise NotImplementedError


class Data(object):

    def __init__(self, temperature, brightness, video_filename):
        self.temperature = temperature
        self.brightness = brightness
        self.filename = video_filename

    def upload_measurements(self):
        get_params = {
            'temperature': self.temperature,
            'brightness': self.brightness,
            'video': self.filename
        }
        urlopen(r'http://axelingolfsson.com/tolvel/set_measurements/?%s' %
                urlencode(get_params))
