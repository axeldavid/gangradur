#!../bin/python
from gangradur import Brightness, Data, Motor, Temperature, Video


def __main__():
    webcam = Video()
    heat_sensor = Temperature()
    brightness_sensor = Brightness()
    motor = Motor()

    webcam.record()
    motor.forward(time=10)  # 10 seconds
    temperature = heat_sensor.get_temperature()
    brightness = brightness_sensor.get_brightness()
    motor.backwards(time=10)
    webcam.wait_until_recording_finishes()
    webcam.compress_and_upload_video()

    data = Data(temperature, brightness, webcam.oggfile)
    data.upload_measurements()

if __name__ == '__main__':
    __main__()
